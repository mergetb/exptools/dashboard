# Dashboard

This is the documentation for the Dashboard, a separate portal from the Launch application for dashboard project. The Dashboard is built using the Laravel framework and utilizes React, JavaScript, and TypeScript.

## Table of Contents

- [Objective](#objective)
- [Framework and Languages](#framework-and-languages)
- [Routes](#routes)
- [File Specific Information](#file-specific-information)
- [Style](#style)
- [References](#references)
- [Environment Setup](#environment-setup)

## Objective

The Dashboard's objective is to provide a separate portal from the Launch application, offering users an interface to interact with their data and visualize it.

## Framework and Languages

- Framework: Laravel
- Languages: React, JavaScript, and TypeScript

## Routes

Location: `/routes/web.php`

- `/` - The index route, which calls for authorization endpoints through the AuthController from `app/Http/Controller/AuthController`.
  - Loads `resources/view/auth/login`
  - `/resources/js/components/Login.jsx` is a JSX file that calls `auth.dorvan.mergetb.net` endpoints for authentication.
- `/dashboard` - Loads `resources/view/auth/dashboard` if the user is authorized successfully.

## File Specific Information

### Dashboard

Location: `/resources/js/components/Dashboards.jsx`

- Sends selected Model Name from Home, Enterprise, or WAN to the Network component for generating the D3 Graph.
- Uses localized date from `model_types.ts` file for the materialize status date information.

### D3 Graph

Location: `/resources/js/components/Network.jsx`

- Handles everything related to the view of the network graph in the popup of View Topology.
- The radio buttons are handled in the Dashboard component.
- Launch Portal Reference: [Launch GitLab Repository](https://gitlab.com/mergetb/portal/launch/-/tree/main/src/app/Revision)

## Style

Location: `/resources/css/dashboard.css`

- Single CSS file for custom styles used in the Dashboard.

## References

- MergeTB Experimenter API: [API Documentation](https://mergetb.gitlab.io/api/)
- Launch portal: [Launch Portal](https://auth.dorvan.mergetb.net/.login/web/auth/login)
- Launch Portal GitLab: [Launch GitLab Repository](https://gitlab.com/mergetb/portal/launch)

## Environment Setup

- Run "php artisan serve", src as root folder
- Run "npm run dev" in bash, src as root folder
