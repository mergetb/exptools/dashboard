<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use GuzzleHttp\Client;

class AuthController extends Controller
{
    public function index()
    {
        return view('auth.login');
    } 

    public function dashboard()
    {
        return view('auth.dashboard');
    }
    
}