import React from 'react';
import ReactDOM from 'react-dom';
import RadioButtons from './components/Dashboards';

ReactDOM.render(<RadioButtons />, document.getElementById('dashboard'));
