import React, {useState, useEffect} from "react";
import ReactDOM from "react-dom/client";
import Network from "./Network";
import axios from "axios";
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faCheckCircle, faExclamationCircle, faSyncAlt, faSpinner } from '@fortawesome/free-solid-svg-icons';
import { LocalizeDate } from './model_types';
import { Prism as SyntaxHighlighter } from 'react-syntax-highlighter';
import { vscDarkPlus as textStyle } from 'react-syntax-highlighter/dist/esm/styles/prism';

// Extra themes for model viewer from highlighter
// coldarkCold
// vscDarkPlus
// coyWithoutShadows
// base16AteliersulphurpoolLight

class Dashboard extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            userData:[],
            lastStatus: "No",
            value: 3,
            fSize: 20,
            selectedModel: "nodes",
            selectedTopology: "2d",
            selectedModelName: "home",
            model: [],
            project: [],
            username: "",
            statusData: {},
            showStatusData: false,
            intervalId: null,
            isExperimentStarted: false,
            isMtzCreated: false,
            isXDCreated: false,
            status: {
              status: null,
              highestStatus: null,
              goal: {
                name: null,
                desc: null,
                type: null
              },
              lastUpdated: null
            },
            XDCsURL: "",
            attachXDCEr: "",
        };
        this.handleModelChange = this.handleModelChange.bind(this);
        this.handleTopologyChange = this.handleTopologyChange.bind(this);
        this.handleModelNameChange = this.handleModelNameChange.bind(this);
    }

    // This function handles changes to the selected model from the redio button and updates the state with the new value (Nodes, LAN, Interfaces).
    handleModelChange(event) {
        this.setState({ selectedModel: event.target.value });
    }

    // This function handles changes to the selected topology from the redio button and updates the state with the new value (2d, 3d). 
    handleTopologyChange(event) {
        this.setState({ selectedTopology: event.target.value });
    }
 
    // This function handles changes to the selected model name (home, enterprise, wan) from the redio button and updates the state with the new value. 
    handleModelNameChange(event) {
        this.setState({ selectedModelName: event.target.value });
    }

    // This function sends a request to an authentication server to retrieve the currently authenticated user's information. It then sets the retrieved user data and username as state variables. If the request fails, it logs the error to the console.
    authenticate() {
      fetch('https://auth.dorvan.mergetb.net/.ory/kratos/sessions/whoami', { mode:'cors', credentials: "include"})
      .then(response => response.json())
      .then(data => {
        this.state.userData = data;
        this.state.username = data.identity.traits.username;
      })
      .catch(error => {
        console.log(error);
      });
    }

    // Get ModelFile (python code) for the selected model name
    data() {
      axios
        .get('https://api.dorvan.mergetb.net/project/dashboard/experiment/'+this.state.selectedModelName+'/revision/aad86245c40a248420f871e8968e65736964447c', { withCredentials: true })
        .then(response => {
            this.setState({model: response.data})
        })
        .catch(error => {
            console.log(error)
        });
    }

    // State the logout and load the home page (authentication page) again
    handleLogout = () => {
        this.logout();
    }

    logout() {
      fetch('https://auth.dorvan.mergetb.net/.ory/kratos/self-service/browser/flows/logout?return_to=http://127.0.0.1:8000/', { mode:'cors', credentials: "include"});
      window.location.href = "http://127.0.0.1:8000/"
    }

    // On clicking the start experiment button call the function to create the realization
    handleExperimentStart = () => {
      this.startExp();
    }
    
    // Create realization by username and call function for creating the materialization after 500 mini seconds
    startExp() {
      fetch('https://api.dorvan.mergetb.net/realize/realizations/dashboard/'+this.state.selectedModelName+'/'+this.state.username, { method:"PUT", credentials: 'include', body: JSON.stringify({revision: "aad86245c40a248420f871e8968e65736964447c"})});
      setTimeout(() => {
      this.startMaterialize();
      }, 3000);
      this.setState({ isExperimentStarted: true });
    }

    // async startExp() {
    //   try {
    //     const response = await fetch('https://api.dorvan.mergetb.net/realize/realizations/dashboard/'+this.state.selectedModelName+'/'+this.state.username, { method:"PUT", credentials: 'include', body: JSON.stringify({revision: "aad86245c40a248420f871e8968e65736964447c"})});
    //     await new Promise(resolve => setTimeout(resolve, 3000));
    //     await this.startMaterialize();
    //     this.setState({ isExperimentStarted: true });
    //   } catch (error) {
    //     console.log(error);
    //   }
    // }
    
    // async startMaterialize() {
    //   try {
    //     const response = await fetch('https://api.dorvan.mergetb.net/materialize/materialize/dashboard/'+this.state.selectedModelName+'/'+this.state.username, { method:"PUT", credentials: 'include'});
    //     await new Promise(resolve => setTimeout(resolve, 10));
    //     await this.matStatus();
    //   } catch (error) {
    //     console.log(error);
    //   }
    // }
    
    // Create materialization by user's name and call for status
    startMaterialize() {
      fetch('https://api.dorvan.mergetb.net/materialize/materialize/dashboard/'+this.state.selectedModelName+'/'+this.state.username, { method:"PUT", credentials: 'include'});
      this.setState({ isMtzCreated: true });
      setTimeout(() => {
        this.matStatus();
        }, 10);
    }

    // Getting status for the material and call countinously every 100 mini seconds to check the status update(Eg. Processing, Pending, Success, Error)
    matStatus() {
      this.state.intervalId = setInterval(() => {
        axios
          .get('https://api.dorvan.mergetb.net/materialize/status/dashboard/'+this.state.selectedModelName+'/'+this.state.username, { withCredentials: true })
          .then(response => {
              const { status, HighestStatus, Goal, LastUpdated } = response.data.status;
              this.setState(prevState => ({
                status: {
                  ...prevState.status,
                  status,
                  highestStatus: HighestStatus,
                  goal: {
                    name: Goal.Name,
                    desc: Goal.Desc,
                    type: Goal.Type
                  },
                  lastUpdated: LastUpdated
                },
                showStatusData: true
              }), () => {
                if (this.state.status.highestStatus === "Success" || this.state.status.highestStatus === "Error") {
                  clearInterval(this.state.intervalId);
                  this.lastTime();
                }
              });
          })
          .catch(error => {
              console.log(error);
              clearInterval(this.state.intervalId);
          });
      }, 100);
    }
    
    // Update the final status for example Success or Error after status stops changing
    async lastTime() {
      try {
        const response = await axios.get('https://api.dorvan.mergetb.net/materialize/status/dashboard/'+this.state.selectedModelName+'/'+this.state.username, { withCredentials: true });
        const { status, HighestStatus, Goal, LastUpdated } = response.data.status;
        this.setState(prevState => ({
          status: {
            ...prevState.status,
            status,
            highestStatus: HighestStatus,
            goal: {
              name: Goal.Name,
              desc: Goal.Desc,
              type: Goal.Type
            },
            lastUpdated: LastUpdated
          },
          showStatusData: true,
          lastStatus: HighestStatus
        }), () => {
          // After getting the status successfully call the createXDC for creating the XDC
          this.createXDC();
          this.setURL();
        });
      } catch (error) {
        console.log(error);
      }
    }

    // Creat XDC using current username
    createXDC(){
      if(this.state.isMtzCreated === true)
      {
        fetch('https://api.dorvan.mergetb.net/xdc/instance/'+this.state.username+'/dashboard', { method:"PUT", credentials: 'include'})
        .then(response => {
          this.setState({ isXDCreated: true });
          setTimeout(() => {
            this.attachXDC();
          }, 10000);
        })
      }
    }

    // Attach XDC to Materialization
    attachXDC(){
      if(this.state.isXDCreated === true)
      {
        fetch('https://api.dorvan.mergetb.net/xdc/attach/'+this.state.username+'/dashboard/'+this.state.selectedModelName+'/'+this.state.username, { method:"PUT", credentials: 'include'})
        .then(response => response.json())
        .then(data => {
          this.setURL();
        })
        .catch(error => {
          console.log(error);
          this.state.attachXDCEr = error;
        });
      }

    }

    // set url value for embbedding in frontend
    setURL(){
      fetch('https://api.dorvan.mergetb.net/xdc/list/dashboard', { credentials: 'include'})
      .then(resp => resp.json())
      .then(result => {
        const xdc = result.XDCs.find((xdc) => xdc.name === this.state.username + ".dashboard");
        if (xdc) {
          this.setState({ XDCsURL: xdc.url });
        }
      })
    }

    // On clicking the stop experiment button call the stopExp function and set the isExperimentStarted value as false
    handleExperimentStop = () => {
        this.stopExp();
        this.setState({ isExperimentStarted: false });
    }

    // Delete materialization, realization and dettach XDC from materialization before deleting it
    stopExp() {
      fetch('https://api.dorvan.mergetb.net/materialize/materialize/dashboard/'+this.state.selectedModelName+'/'+this.state.username, { method:"DELETE", credentials: 'include'}); 
      fetch('https://api.dorvan.mergetb.net/realize/realizations/dashboard/'+this.state.selectedModelName+'/'+this.state.username, { method:"DELETE", credentials: 'include', body: JSON.stringify({revision: "aad86245c40a248420f871e8968e65736964447c"})});
      fetch('https://api.dorvan.mergetb.net/xdc/attach/'+this.state.username+'/dashboard', { method:"DELETE", credentials: 'include'});
      setTimeout(() => {
        this.deleteXDC();
      }, 5000);
      this.setState({ showStatusData: false });
      this.setState({ isMtzCreated: false });
      this.setState({ isXDCreated: false });
    }

    // Delete XDC after 5 seconds of dettaching the XDC
    deleteXDC() {
      fetch('https://api.dorvan.mergetb.net/xdc/instance/'+this.state.username+'/dashboard', { method:"DELETE", credentials: 'include'});
    }

    render() {
      const codeString = this.state.model.ModelFile;
      return(
        <div className="main">
          <div className="page">
            <header className="navbar navbar-expand-md navbar-light d-print-none">
              <div className="container-xl">
                <button className="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbar-menu" aria-controls="navbar-menu" aria-expanded="false" aria-label="Toggle navigation">
                  <span className="navbar-toggler-icon"></span>
                </button>
                <h1 className="navbar-brand navbar-brand-autodark d-none-navbar-horizontal pe-0 pe-md-3">
                  <a href=".">
                    <img src="/images/logo.svg" width="110" height="32" alt="Tabler" className="navbar-brand-image" />
                  </a>
                </h1>
                <div className="navbar-nav flex-row order-md-last">
                  <div className="nav-item dropdown">
                    <a href="#" className="nav-link d-flex lh-1 text-reset p-0" data-bs-toggle="dropdown" aria-label="Open user menu">
                      <span className="avatar avatar-sm">
                        <svg xmlns="http://www.w3.org/2000/svg" width="35" height="35" viewBox="0 0 24 24" fill="none" stroke="#000000" strokeWidth="2" strokeLinecap="round" strokeLinejoin="round">
                          <path d="M5.52 19c.64-2.2 1.84-3 3.22-3h6.52c1.38 0 2.58.8 3.22 3" />
                          <circle cx="12" cy="10" r="3" />
                          <circle cx="12" cy="12" r="10" />
                        </svg>
                      </span>
                      <div className="d-none d-xl-block ps-2">
                        <div id="username">{this.state.username}</div>
                        <div className="mt-1 small text-muted" id="userStatus">Active</div>
                      </div>
                    </a>
                    <div className="dropdown-menu dropdown-menu-end dropdown-menu-arrow">
                      <a className="dropdown-item" id="logout" onClick={this.handleLogout}>Logout</a>
                    </div>
                  </div>
                </div>
              </div>
            </header>
            <div className="page-wrapper mt-3">
              <div className="page-header d-print-none">
                <div className="container-xl">
                  <div className="row g-2 align-items-center">
                    <div className="col">
                      <div className="page-pretitle"> Overview </div>
                      <h2 className="page-title"> Dashboard </h2>
                    </div>
                    <div className="col-auto ms-auto d-print-none">
                      <div className="btn-list">
                        <a href="#" className="btn btn-primary d-none d-sm-inline-block" data-bs-toggle="modal" data-bs-target="#staticBackdrop">
                          <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="#ffffff" strokeWidth="2.5" strokeLinecap="round" strokeLinejoin="round">
                            <path d="M1 12s4-8 11-8 11 8 11 8-4 8-11 8-11-8-11-8z"></path>
                            <circle cx="12" cy="12" r="3"></circle>
                          </svg> View Topology </a>
                        <a href="#" className="btn btn-primary d-sm-none btn-icon" data-bs-toggle="modal" data-bs-target="#model-report" aria-label="Create new report">
                          <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="#ffffff" strokeWidth="2.5" strokeLinecap="round" strokeLinejoin="round">
                            <path d="M1 12s4-8 11-8 11 8 11 8-4 8-11 8-11-8-11-8z"></path>
                            <circle cx="12" cy="12" r="3"></circle>
                          </svg>
                        </a>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
              <div className="page-body mt-2">
                <div className="container-xl">
                  <div className="row row-cards row-deck gy-3 gx-4">
                    <div className="col-sm-6 col-lg-3">
                      <div className="card">
                        <div className="card-body heightFirstRow">
                          <div className="h4 mb-3">Model Selector</div>
                          <div className="dropdown-divider"></div>
                          <div className="d-flex mb-2">
                            <div className="radioform">
                              <label>
                                <input
                                  type="radio"
                                  name="network"
                                  id="home"
                                  value="home"
                                  checked={this.state.selectedModelName === "home"}
                                  onChange={this.handleModelNameChange}
                                />
                                <span>Home Network</span>
                              </label>
                              <label>
                                <input
                                  type="radio"
                                  name="network"
                                  id="enterprise"
                                  value="enterprise"
                                  checked={this.state.selectedModelName === "enterprise"}
                                  onChange={this.handleModelNameChange}
                                />
                                <span>Enterprise Network</span>
                              </label>
                              <label>
                                <input
                                  type="radio"
                                  name="network"
                                  id="wan"
                                  value="wan"
                                  checked={this.state.selectedModelName === "wan"}
                                  onChange={this.handleModelNameChange}
                                />
                                <span>Wide Area Network</span>
                              </label>
                            </div>
                          </div>
                        </div>
                      </div>
                    </div>
                    <div className="col-sm-6 col-lg-3">
                      <div className="card">
                        <div className="card-body heightFirstRow">
                          <div className="h4 mb-3">Experiment Setup</div>
                          <div className="dropdown-divider"></div>
                          <div className="d-flex mb-2">
                            <div className="btn-list expbuttons">
                              <button type="button" className="btn btn-primary mt-3" id="startRealization" onClick={this.handleExperimentStart}>
                                <svg xmlns="http://www.w3.org/2000/svg" className="icon" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="#ffffff" strokeWidth="2" strokeLinecap="round" strokeLinejoin="round"><circle cx="12" cy="12" r="10"></circle><polygon points="10 8 16 12 10 16 10 8"></polygon></svg>
                                Start Experiment
                              </button>
                              <button type="button" className="btn btn-primary mt-4" id="stopRealization" onClick={this.handleExperimentStop}>
                                <svg xmlns="http://www.w3.org/2000/svg" className="icon" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="#ffffff" strokeWidth="2" strokeLinecap="round" strokeLinejoin="round"><circle cx="12" cy="12" r="10"></circle><rect x="9" y="9" width="6" height="6"></rect></svg>
                                Stop Experiment
                              </button>
                            </div>
                          </div>
                        </div>
                      </div>
                    </div>
                    <div className="col-md-6 col-lg-6">
                      <div className="card">
                        <div className="card-body heightFirstRow">
                          <div className="h4 mb-3">Status</div>
                          <div className="dropdown-divider"></div>
                          <div className="d-flex mb-2">
                            <div className="card-body py-2">
                              <div className="status">
                              {this.state.showStatusData ? (
                                <div className="status-data">
                                  <span>Status: </span>
                                  {this.state.status.highestStatus === "Success" ? (
                                    <span className="text-success">
                                      <FontAwesomeIcon icon={faCheckCircle} /> Success
                                    </span>
                                  ) : this.state.status.highestStatus === "Processing" ? (
                                    <span className="text-info">
                                      <FontAwesomeIcon icon={faSyncAlt} /> Processing
                                    </span>
                                  ) : this.state.status.highestStatus === "Pending" ? (
                                    <span className="text-warning">
                                      <FontAwesomeIcon icon={faSpinner} /> Pending
                                    </span>
                                  ) : (
                                    <span className="text-danger">
                                      <FontAwesomeIcon icon={faExclamationCircle} /> Error
                                    </span>
                                  )}
                                  <br></br>
                                  <span>Name:</span>
                                  <span> {this.state.status.goal.name} </span>
                                  <br></br>
                                  <span>Description: </span>
                                  <span> {this.state.status.goal.desc} </span>
                                  <br></br>
                                  <span>Type:</span>
                                  <span> {this.state.status.goal.type} </span>
                                  <br></br>
                                  <span>Last Updated:</span>
                                  <span> {LocalizeDate(this.state.status.lastUpdated)} </span>
                                </div>
                              ) : (
                                <div className="status-message">
                                  <span>Status: </span>
                                  <span className="text-danger">
                                    <FontAwesomeIcon icon={faExclamationCircle} /> Error
                                  </span>
                                  <br></br>
                                  <span>Description: </span>
                                  <span> Waiting for experiment to start!! </span>
                                </div>
                              )}
                            </div>

                            </div> 
                          </div>
                        </div>
                      </div>
                    </div>
                    <div className="col-md-6 col-lg-4">
                      <div className="card heightSecondRow">
                        <div className="card-header">
                          <h3 className="card-title">Model Viewer</h3>
                        </div>
                        <div className="pymodel">
                          <SyntaxHighlighter language="python" style={textStyle} wrapLongLines={true} customStyle={{height: '100%'}}>
                            {codeString}
                          </SyntaxHighlighter>
                        </div>
                      </div>
                    </div>
                    <div className="col-md-12 col-lg-8 mb-5">
                      <div className="card heightSecondRow">
                        <div className="card-header">
                          <h3 className="card-title">XDC</h3>
                        </div>
                        <div className="status">
                          {this.state.showStatusData ? (
                            <div className="XDC">
                              {this.state.attachXDCEr === "" ? (
                                <div>
                                  <iframe src={this.state.XDCsURL} width="100%" height="500"></iframe>
                                </div>
                              ) : ( 
                                <div className="status-message text-center text-danger">
                                <br></br>
                                <span><FontAwesomeIcon icon={faExclamationCircle} /></span>
                                <span> {this.state.attachXDCEr} </span>
                              </div>
                              )}
                            </div>

                          ) : (
                            <div className="status-message text-center text-warning">
                              <br></br>
                              <span><FontAwesomeIcon icon={faExclamationCircle} /></span>
                              <span> Not able to create or attach XDCs Instance </span>
                            </div>
                          )}
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
          <div className="modal fade" id="staticBackdrop" data-bs-backdrop="static" data-bs-keyboard="false" tabIndex="-1" aria-labelledby="staticBackdropLabel" aria-hidden="true">
            <div className="modal-dialog modal-xl">
              <div className="modal-content">
                <div className="modal-header">
                  <h5 className="modal-title" id="staticBackdropLabel">Topology</h5>
                  <button type="button" className="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                </div>
                <div className="modal-body">
                  <div className="row">
                    <div className="container-fluid">
                      <div className="row">
                          <div className="col-lg-3">
                              <div className="model mb-3">
                                  <div className="radioClass fw-bold mb-2">Model View</div>
                                  <div className="form-check">
                                      <input
                                          className="form-check-input"
                                          type="radio"
                                          name="model"
                                          id="model1"
                                          value="nodes"
                                          checked={this.state.selectedModel === "nodes"}
                                          onChange={this.handleModelChange}
                                      />
                                      <label className="form-check-label" htmlFor="model1">
                                          Nodes
                                      </label>
                                  </div>
                                  <div className="form-check">
                                      <input
                                          className="form-check-input"
                                          type="radio"
                                          name="model"
                                          id="model2"
                                          value="lans"
                                          checked={this.state.selectedModel === "lans"}
                                          onChange={this.handleModelChange}
                                      />
                                      <label className="form-check-label" htmlFor="model2">
                                          Nodes & LANs
                                      </label>
                                  </div>
                                  <div className="form-check">
                                      <input
                                          className="form-check-input"
                                          type="radio"
                                          name="model"
                                          id="model3"
                                          value="interfaces"
                                          checked={this.state.selectedModel === "interfaces"}
                                          onChange={this.handleModelChange}
                                      />
                                      <label className="form-check-label" htmlFor="model3">
                                          Nodes & Interfaces
                                      </label>
                                  </div>
                              </div>
                              <div className="topology mb-3">
                                  <div className="radioClass fw-bold mb-2">Topology View</div>
                                  <div className="form-check">
                                      <input
                                          className="form-check-input"
                                          type="radio"
                                          name="topology"
                                          id="topology1"
                                          value="2d"
                                          checked={this.state.selectedTopology === "2d"}
                                          onChange={this.handleTopologyChange}
                                      />
                                      <label className="form-check-label" htmlFor="topology1">
                                          2D
                                      </label>
                                  </div>
                                  <div className="form-check">
                                      <input
                                          className="form-check-input"
                                          type="radio"
                                          name="topology"
                                          id="topology2"
                                          value="3d"
                                          checked={this.state.selectedTopology === "3d"}
                                          onChange={this.handleTopologyChange}
                                      />
                                      <label className="form-check-label" htmlFor="topology2">
                                          3D
                                      </label>
                                  </div>
                              </div>
                              <div className="slider mb-3">
                                  <div className="radioClass fw-bold mb-2">Topology Display</div>
                                  <label htmlFor="nodesize" className="form-label">
                                  Node Size:
                                  <div id="nodeSize" name="nodeSize" value={this.state.value}>
                                      {this.state.value}
                                  </div>
                                  </label>
                                  <input
                                      className="form-range customRange"
                                      type="range"
                                      name="slider"
                                      id="slider"
                                      min={0}
                                      max={10}
                                      value={this.state.value}
                                      onChange={(e) => this.setState({ value: e.target.value })}
                                  />
                                  <div className="d-flex justify-content-between">
                                      <span>0</span>
                                      <span>10</span>
                                  </div>
                                  <label htmlFor="fontsize" className="form-label">
                                  Font Size:
                                  <div id="fontsize" name="fontsize" value={this.state.fSize}>
                                      {this.state.fSize}
                                  </div>
                                  </label>
                                  <input
                                      className="form-range customRange"
                                      type="range"
                                      name="sliderFont"
                                      id="sliderFont"
                                      min={1}
                                      max={50}
                                      value={this.state.fSize}
                                      onChange={(e) => this.setState({ fSize: e.target.value })}
                                  />
                                  <div className="d-flex justify-content-between">
                                      <span>1</span>
                                      <span>50</span>
                                  </div>
                              </div>
                          </div>
                          <div className="col-lg-9">
                          <Network
                              selectedModel={this.state.selectedModel}
                              selectedTopology={this.state.selectedTopology}
                              value={this.state.value}
                              fSize={this.state.fSize}
                              selectedModelName={this.state.selectedModelName}
                          />
                          </div>
                      </div>
                    </div>
                  </div>
                </div>
                <div className="modal-footer">
                  <button type="button" className="btn btn-secondary" data-bs-dismiss="modal">Close</button>
                </div>
              </div>
            </div>
          </div>
        </div>
      );
    }

    componentDidMount(){
      this.data();
      this.matStatus();
      this.authenticate();
      this.setURL();
    }
}

const root = document.getElementById("dashboard");
ReactDOM.createRoot(root).render(<Dashboard />);