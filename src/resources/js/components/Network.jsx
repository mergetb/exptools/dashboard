import React, {useState, useEffect} from "react";
import { ForceGraph2D, ForceGraph3D, ForceGraphVR } from 'react-force-graph';
import axios from "axios";
import * as d3 from "d3";
import * as ModelTypes from "./model_types";
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faExclamationCircle } from '@fortawesome/free-solid-svg-icons';


function Network(props) {

    const [posts, setPosts] = useState([]);
    const [error, setError] = useState(null);
    const [loading, setLoading] = useState(true);

    useEffect(() => {
        axios
            .get('https://api.dorvan.mergetb.net/project/dashboard/experiment/'+props.selectedModelName, { withCredentials: true })
            .then(response => {
                if (response && response.data && response.data.experiment && response.data.experiment.models) {
                    const models = response.data.experiment.models;
                    const revisionID = Object.keys(models)[0];
                    axios
                        .get('https://api.dorvan.mergetb.net/project/dashboard/experiment/'+props.selectedModelName+'/revision/'+revisionID, { withCredentials: true })
                        .then(response => {
                            setPosts(response.data);
                            setLoading(false);
                            setError(null);
                        })
                        .catch(error => {
                            console.log(error);
                            setError(error);
                            setLoading(false);
                        });
                }
            })
            .catch(error => {
                console.log(error);
                setError(error);
                setLoading(false);
            });
    }, [props.selectedModelName]);

    let nodes = [];
    let links = [];

    if(props.selectedModel === "nodes")
    {
        if(posts.model)
        {
            if(posts.model.model)
            {
                if (posts.model.model.nodes) 
                {
                    for (let i = 0; i < posts.model.model.nodes.length; i++) 
                    {
                        nodes.push({
                            id: posts.model.model.nodes[i].id,
                            type: 'node',
                            label: posts.model.model.nodes[i].id,
                            xir: posts.model.model.nodes[i],
                        });
                    }
                }
            
                if (posts.model.model.links) 
                {
                    for (let li = 0; li < posts.model.model.links.length; li++) 
                    {
                        for (let i = 0; i < posts.model.model.links[li].endpoints.length; i++) 
                        {
                            for (let j = 0; j < posts.model.model.links[li].endpoints.length; j++) 
                            {
                                if (i == j) 
                                {
                                    continue;
                                }
                    
                                let n1 = posts.model.model.links[li].endpoints[i].socket.element;
                                let n2 = posts.model.model.links[li].endpoints[j].socket.element;
                        
                                links.push({
                                    id: n1 + '~' + n2,
                                    source: n1,
                                    target: n2,
                                    xir: posts.model.model.links[li],
                                });
                            }
                        }
                    }
                }    
            }
        }       
    }
    else
    {
        if (props.selectedModel === "lans")
        {
            if(posts.model)
            {
                if(posts.model.model)
                {
                    if (posts.model.model.nodes) {
                        for (let i = 0; i < posts.model.model.nodes.length; i++) {
                            nodes.push({
                                id: posts.model.model.nodes[i].id,
                                type: 'node',
                                label: posts.model.model.nodes[i].id,
                                xir: posts.model.model.nodes[i],
                            });
                        }
                    }
            
                    if (posts.model.model.links) {
                        for (let i = 0; i < posts.model.model.links.length; i++) {
                            if (posts.model.model.links[i].endpoints.length > 2) {
                                nodes.push({
                                    id: posts.model.model.links[i].id,
                                    type: 'LAN',
                                    label: 'LAN',
                                    xir: posts.model.model.links[i],
                                });
                            }
                        }
                        for (let l = 0; l < posts.model.model.links.length; l++) {
                            for (let i = 0; i < posts.model.model.links[l].endpoints.length; i++) {
                                for (let j = 0; j < posts.model.model.links[l].endpoints.length; j++) {
                                    if (i == j) {
                                    continue;
                                    }
                        
                                    const n1 = posts.model.model.links[l].endpoints[i].socket.element;
                                    let n2 = '';
                        
                                    if (posts.model.model.links[l].endpoints.length == 2) {
                                    n2 = posts.model.model.links[l].endpoints[j].socket.element;
                                    } else {
                                    n2 = posts.model.model.links[l].id;
                                    }
                        
                                    links.push({
                                    id: n1 + '~' + n2,
                                    source: n1,
                                    target: n2,
                                    xir: links[l],
                                    });
                                }
                            }
                        }      
                    }
                }
            }
        }
        else
        {
            if (props.selectedModel === "interfaces")
            {
                if(posts.model)
                {
                    if(posts.model.model)
                    {
                        if (posts.model.model.nodes) {
                            for (let i = 0; i < posts.model.model.nodes.length; i++) {
                                nodes.push({
                                    id: posts.model.model.nodes[i].id,
                                    type: 'node',
                                    label: posts.model.model.nodes[i].id,
                                    xir: posts.model.model.nodes[i],
                                });
                        
                                if (posts.model.model.nodes[i].sockets) 
                                {
                                    for (let j = 0; j < posts.model.model.nodes[i].sockets.length; j++) 
                                    {
                                        let lab = posts.model.model.nodes[i].id + '.' + posts.model.model.nodes[i].sockets[j].index;
                                        if (posts.model.model.nodes[i].sockets[j].addrs && posts.model.model.nodes[i].sockets[j].addrs.length != 0) 
                                        {
                                            lab = posts.model.model.nodes[i].sockets[j].addrs.join('/');
                                        }
                            
                                        const socketid = posts.model.model.nodes[i].id + '.' + posts.model.model.nodes[i].sockets[j].index;
                            
                                        nodes.push({
                                            id: socketid,
                                            type: 'socket',
                                            label: lab,
                                            xir: posts.model.model.nodes[i].sockets[j],
                                        });
                            
                                        links.push({
                                            id: posts.model.model.nodes[i].id + '~' + socketid,
                                            source: posts.model.model.nodes[i].id,
                                            target: socketid,
                                            xir: posts.model.model.nodes[i].sockets[j],
                                        });
                                    }
                                }
                            }
                        }
                        
                        if (posts.model.model.links) {
                            for (let li = 0; li < posts.model.model.links.length; li++) {
                            for (let i = 0; i < posts.model.model.links[li].endpoints.length; i++) {
                                for (let j = 0; j < posts.model.model.links[li].endpoints.length; j++) {
                                if (i == j) {
                                    continue;
                                }

                                const sid = posts.model.model.links[li].endpoints[i].socket.element + '.' + posts.model.model.links[li].endpoints[i].socket.index;
                                const tid = posts.model.model.links[li].endpoints[j].socket.element + '.' + posts.model.model.links[li].endpoints[j].socket.index;
                        
                                    links.push({
                                    id: sid + '~' + tid,
                                    source: sid,
                                    target: tid,
                                    xir: posts.model.model.links[li],
                                });
                                }
                            }
                            }
                        }
                    }
                }
            }
        }
    }

    const data = {
        nodes,
        links
    };

    const { useRef } = React;

    const fgRef = useRef();
    const fgRef3d = useRef();


    // Zooming the graph to fit the alloted height and width
    fgRef.current && fgRef.current.zoomToFit(200, 200);
    // Centering the graph
    fgRef.current && fgRef.current.centerAt(-150, -100);

    const drawNode = ({ x, y, type }, ctx) => {
        switch (type) {
          case ModelTypes.NodeType.Node:
            ctx.fillStyle = "green";
            ctx.beginPath();
            ctx.arc(x, y, props.value, 0, 2 * Math.PI, false);
            ctx.fill();
            break;
          case ModelTypes.NodeType.LAN:
            ctx.fillStyle = "gray";
            ctx.beginPath();
            ctx.fillRect(x - props.value*1.5 / 2, y - props.value*1.5 / 2, props.value*1.5, props.value*1.5);
            break;
          case ModelTypes.NodeType.Socket:
            ctx.fillStyle = "red";
            ctx.beginPath(); 
            ctx.moveTo(x, y - 1 * props.value); 
            ctx.lineTo(x - 1 * props.value, y + 1 * props.value); 
            ctx.lineTo(x + 1 * props.value, y + 1 * props.value); 
            ctx.fill();
        }
    };

    const onNodeLabel = (n) => {
        let name = '';
        let metal = '';
    
        switch (n.type) {
          case ModelTypes.NodeType.Node:
            name = n.label;
            metal = n.xir.metal ? n.xir.metal.value : 'false';
            break;
          case ModelTypes.NodeType.LAN:
            name = n.id;
            metal = 'N/A';
            break;
          case ModelTypes.NodeType.Socket:
            name = n.label;
            metal = 'N/A';
            break;
        }
    
        return `
                <table>
                    <style>
                    table, th, td {
                        padding: 5px;
                    }
                    tr {
                        border-bottom: 1px solid #ddd;
                    }
                    </style>
                    <tr>
                        <th>Name</th>
                        <th>Metal</th>
                    </tr>
                    <tr>
                        <td>${name}</td>
                        <td>${metal}</td>
                    </tr>
                </table>
            `;
    };
    

    if(error)
    {
        return (
            <div className="App">
                <span className="text-danger">
                    <FontAwesomeIcon icon={faExclamationCircle} />
                </span>
                <span>An error occurred: {error.message}</span>
            </div>
        );
    }
    else
    {
        if(props.selectedTopology === "2d")
        {
            return (
                <div className="App">
                    <div>{props.naming}</div>
                    <div className="graph-container">
                        <ForceGraph2D
                            ref={fgRef}
                            graphData={data}
                            nodeLabel={onNodeLabel}
                            linkLabel="id"
                            linkDirectionalParticleColor={() => "red"}
                            linkDirectionalParticleWidth={6}
                            linkHoverPrecision={10}
                            onNodeDragEnd={(node) => {
                            node.fx = node.x;
                            node.fy = node.y;
                            node.fz = node.z;
                            }}
                            width={800}
                            height={600}
                            showNavInfo={true}
                            nodeCanvasObject={(node, ctx, globalScale) => {
                                const label = node.label;
                                const fontSize = (props.fSize)/globalScale;
                                ctx.font = `${fontSize}px Sans-Serif`;
                                const textWidth = ctx.measureText(label).width;
                                const bckgDimensions = [textWidth, fontSize].map(n => n + fontSize * 0.2);
                    
                                ctx.fillStyle = 'rgba(255, 255, 255, 0.3)';
                                ctx.fillRect(node.x - bckgDimensions[0] / 2, node.y - (props.value +5) - bckgDimensions[1] / 2, ...bckgDimensions);
                    
                                ctx.textAlign = 'center';
                                ctx.textBaseline = 'middle';
                                ctx.fillStyle = "black";
                                ctx.fillText(label, node.x - (props.value), node.y - (props.value));
                                
                                drawNode(node, ctx);
                                node.__bckgDimensions = bckgDimensions;

                            }}
                            cooldownTicks={75}
                            onEngineStop={() => fgRef.current.zoomToFit(300)}
                            onLinkClick={(link) => fgRef.current.emitParticle(link)}
                        />
                        </div>
                </div>
            );
        }
        else
        {
            return (
                <div className="App chart">
                <ForceGraph3D
                    ref={fgRef3d}
                    graphData={data}
                    nodeLabel="label"
                    linkLabel="label"
                    linkDirectionalParticleColor={() => "red"}
                    linkDirectionalParticleWidth={6}
                    linkHoverPrecision={10}
                    onNodeDragEnd={(node) => {
                    node.fx = node.x;
                    node.fy = node.y;
                    node.fz = node.z;
                    }}
                    width={800}
                    height={600}
                    showNavInfo={true}
                    onLinkClick={(link) => fgRef.current.emitParticle(link)}
                />
                </div>
            );
        }
    }
  
}

export default Network;
