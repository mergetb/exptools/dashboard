import React, { useState, useEffect } from 'react';


  
  const Login = () => {
    const [statusCode, setStatusCode] = useState(null);
    useEffect(() => {
      window.location.href = 'http://auth.dorvan.mergetb.net/.login/web/auth/login?return_to=http://127.0.0.1:8000/dashboard';
    }, []);

    useEffect(() => {
        fetch('https://api.dorvan.mergetb.net/experiment', { mode:'cors', credentials: 'include'})
          .then(response => {
            setStatusCode(response.status);
            if (response.status == 401) {
                window.location.href = 'http://auth.dorvan.mergetb.net/.login/web/auth/login?return_to=http://127.0.0.1:8000/dashboard';
            }
            });
        }, []);
  
    return null;
  };
  
export default Login;
